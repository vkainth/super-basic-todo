let addBtn = document.getElementById('add');
let itemName = document.getElementById('item');
let itemsList = document.getElementById('itemsList');

function addItem(inputTag, outputList) {
    let checked = false;
    let div = document.createElement('div');
    let btnDiv = document.createElement('div');
    let liDiv = document.createElement('div');
    let listItem = document.createElement('li');
    let deleteButton = document.createElement('button');

    div.classList.add('row');
    div.classList.add('justify-content-center');
    btnDiv.classList.add('col');
    btnDiv.classList.add('text-center');
    liDiv.classList.add('col');
    liDiv.classList.add('text-center');

    deleteButton.classList.add('btn');
    deleteButton.classList.add('btn-danger');
    deleteButton.innerHTML = '<i class="fas fa-times"></i>';

    listItem.appendChild(document.createTextNode(inputTag.value));
    listItem.classList.add('hover');

    listItem.addEventListener('click', () => {
        if (checked === false) {
            listItem.classList.add('done');
            checked = true;
        } else {
            listItem.classList.remove('done');
            checked = false;
        }
    });

    deleteButton.addEventListener('click', () => {
        itemsList.removeChild(div);
    });

    liDiv.appendChild(listItem);
    btnDiv.appendChild(deleteButton);

    div.appendChild(liDiv);
    div.appendChild(btnDiv);

    outputList.appendChild(div);
    inputTag.value = '';
}

addBtn.addEventListener('click', () => {
    if (itemName.value.length > 0) {
        addItem(itemName, itemsList);
    }
});

itemName.addEventListener('keypress', event => {
    if (event.key === 'Enter' && itemName.value.length > 0) {
        addItem(itemName, itemsList);
    }
});
