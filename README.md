# Super Basic ToDo App

[Demo](https://vkainth.gitlab.io/super-basic-todo)

## Introduction

A super-simple, super basic ToDo List Application using the DOM and JavaScript.

## Technologies Used

- HTML and CSS
- JavaScript

## Functionality

- Can add items to a list

- Can strike them out

- Can remove items from list

## Screenshots

![Starting Page](screenshots/screen_1.png 'Starting Page')
![Adding Items](screenshots/screen_2.png 'Adding Items')
![Checking Items](screenshots/screen_3.png 'Checking Items')
